﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shared;
using Zyan.Communication;
using Zyan.Communication.Protocols.Http;

namespace client
{
    class Program
    {
        static void Main(string[] args)
        {
            var protocolSetup = new HttpCustomClientProtocolSetup(true);
            var connection = new ZyanConnection("http://localhost:8080/EchoExample", protocolSetup);

            var proxy = connection.CreateProxy<IEchoComponent>();

            var result = proxy.Echo("Hello, World!");

            Console.Write(result);
            Console.Read();
        }
    }
}
