﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shared;
using Zyan.Communication;
using Zyan.Communication.Protocols.Http;
using Zyan.Communication.Security;

namespace ZyanStudy
{
    class Program
    {
        static void Main(string[] args)
        {
            var protocolSetup = new HttpCustomServerProtocolSetup(8080, new NullAuthenticationProvider(), true);
            var host = new ZyanComponentHost("EchoExample", protocolSetup);

            host.RegisterComponent<IEchoComponent, EchoComponent>(ActivationType.SingleCall);
            Console.Write("service starting");
            Console.Read();
        }
    }
}
